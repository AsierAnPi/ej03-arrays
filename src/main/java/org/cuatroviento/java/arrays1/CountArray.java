/**
 * 
 */
package org.cuatroviento.java.arrays1;

import java.util.Scanner;

/**
 * @author Asgarth
 *
 */
public class CountArray {

	private static Scanner readFromConsole;

	/**
	 * @param args
	 */

		public static void main(String[] args) {
			// TODO Auto-generated method stub
			float [] num = new float[10];
			readFromConsole = new Scanner(System.in);
			String line = "";
			int pos = 0;
			int neg = 0;
			int zer = 0;
			

			for (int i =0; i<num.length; i++){
				System.out.println("Número entero " + i + ": ");
				line = readFromConsole.nextLine();
				num[i] = Float.parseFloat(line);
		}
			for (int i =0; i<num.length; i++){
				if (num[i]>0) {
					pos++;
				} else if (num[i]<0) {
					neg++;
				} else
					zer++;
			}
			System.out.println("Ceros: " + zer + " - Positivos: " +pos+ " - Negativos: " + neg);
		}
		
			
	}



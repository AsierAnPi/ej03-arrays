/**
 * 
 */
package org.cuatroviento.java.arrays1;

import java.util.Scanner;

/**
 * @author Asgarth
 *
 */
public class NameArray {

	private static Scanner readFromConsole;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		readFromConsole = new Scanner(System.in);
		String line = "";
		int [] numeros = new int[10];
		for (int i =0; i<numeros.length; i++){
			System.out.println("Número entero " + i + ": ");
			line = readFromConsole.nextLine();
			numeros[i] = Integer.parseInt(line);
	}
		for (int i =0; i<numeros.length; i++){
			System.out.println("Elemento [" + i + "]: " +numeros[i]);
		}
		}

}

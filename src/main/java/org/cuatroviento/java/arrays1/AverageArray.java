/**
 * 
 */
package org.cuatroviento.java.arrays1;

import java.util.Scanner;

/**
 * @author Asgarth
 *
 */
public class AverageArray {

	private static Scanner readFromConsole;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		float [] num = new float[10];
		readFromConsole = new Scanner(System.in);
		String line = "";
		float suma = 0;
		for (int i =0; i<num.length; i++){
			System.out.println("Número entero " + i + ": ");
			line = readFromConsole.nextLine();
			num[i] = Float.parseFloat(line);
			suma = suma + num[i];
	}
		System.out.println("La media es: " + suma / num.length);
	}
}
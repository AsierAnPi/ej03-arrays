/**
 * 
 */
package org.cuatroviento.java.arrays1;

import java.util.Scanner;

/**
 * @author Asgarth
 *
 */
public class NumberArray {

	private static Scanner readFromConsole;

	/** 
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int [] num = new int[10];
		readFromConsole = new Scanner(System.in);
		String line = "";
		int numero;
		
		for (int i =0; i<num.length; i++){
			System.out.println("Número entero " + i + ": ");
			line = readFromConsole.nextLine();
			num[i] = Integer.parseInt(line);
	}
		for (int i=0; i<num.length;i++){
			numero=num[i];
			for(int j=0; j<num.length;j++){
				if(numero == num[j] && j!=i){
					System.out.println("Hay un número repetido en la posición " + i + " y " + j);
					return;
					//el break; saldría del bucle for de la "j"
				}
			}
		}
	}
}

/**
 * 
 */
package org.cuatroviento.java.arrays1;

import java.util.Scanner;

/**
 * @author Asgarth
 *
 */
public class IncrementArray {

	private static Scanner readFromConsole;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int [] num = new int[10];
		readFromConsole = new Scanner(System.in);
		String line = "";
		
		for (int i =0; i<num.length; i++){
			System.out.println("Número entero " + i + ": ");
			line = readFromConsole.nextLine();
			num[i] = Integer.parseInt(line);
	}
		for (int i =0; i<num.length; i++){
			num[i]++;
			System.out.println("Número entero " + i + " +1:" +num[i]);
	}
	}
}

	
